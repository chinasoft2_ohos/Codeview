/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.protectsoft.webviewcodedemo;

/**
 * Constants
 *
 * @since 2021-04-30
 */
public class Constants {
    /**
     * SHOW_TYPE
     */
    public static final String SHOW_TYPE = "SHOW_TYPE";
    /**
     * SHOW_CODE
     */
    public static final String SHOW_CODE = "SHOW_CODE";
    /**
     * SHOW_LANG
     */
    public static final String SHOW_LANG = "SHOW_LANG";
    /**
     * Basic usage. Default style is Original, and default language is java.
     */
    public static final int DEFALUT_TYPE = 1;
    /**
     * set style and language.
     */
    public static final int OTHER_TYPE = 2;
    /**
     * Inject html head content and text.
     */
    public static final int OTHER1_TYPE = 3;
    /**
     * PYTHON_TYPE
     */
    public static final int PYTHON_TYPE = 4;
    /**
     * JAVASCRIPT_TYPE
     */
    public static final int JAVASCRIPT_TYPE = 5;
    /**
     * RUBY
     */
    public static final int RUBY_TYPE = 6;
    /**
     * CSHARP
     */
    public static final int CSHARP_TYPE = 7;
    /**
     * PHP
     */
    public static final int PHP_TYPE = 8;
    /**
     * SQL
     */
    public static final int SQL_TYPE = 9;
    /**
     * CPLUSPLUS
     */
    public static final int CPLUSPLUS_TYPE = 10;

    /**
     * DEFALUT_STRING
     */
    public static final String DEFALUT_STRING = "public static void main(String[] args) { \n"
            + "\n" + "//comments\n"
            + "   for(int i =0; i < 10; i++) {\n"
            + "       addnum();\n"
            + "   }\n"
            + "\n"
            + "}\n";
    /**
     * OTHER_STRING
     */
    public static final String OTHER_STRING = "public static void main(String[] args) { \n"
            + "\n" + "//comments\n"
            + "   for(int i =0; i < 10; i++) {\n"
            + "       addnum();\n"
            + "   }\n"
            + "\n"
            + "}\n";
    /**
     * OTHER1_STRING
     */
    public static final String OTHER1_STRING = "function Constructor(v1,v2,v3)\n" + "{\n"
            + "  this.v1 = v1;\n"
            + "  this.v2 = v2;\n"
            + "  this.funk = function()\n"
            + "  {\n"
            + "    console.log(\"Test: \"+ v3 );\n"
            + "  }\n"
            + "}\n"
            + "\n"
            + "var obj1 = new Constructor(\"par1\",\"par2\",\"par3\");\n"
            + "var arr = [\"w1\",\"w2\",\"w3\",obj1];\n" + "\n"
            + "function f2()\n" + "{            \n"
            + "  obj1.funk(); //works ok\n"
            + "  console.log(\"test \"+tablica[3].funk.call() ); //doesn't work\n"
            + "}";
    /**
     * PYTHON_STRING
     */
    public static final String PYTHON_STRING = "import random\n" +
            "if __name__ ==\"__main__\":\n" +
            "    checkcode=\"\"\n" +
            "    for i in range(4):\n" +
            "        index=random.randrange(0,4)\n" +
            "        if index!=i and index +1 !=i:\n" +
            "            checkcode +=chr(random.randint(97,122))\n" +
            "        elif index +1==i:\n" +
            "            checkcode +=chr(random.randint(65,90) )\n" +
            "        else:\n" +
            "            checkcode +=str(random.randint(1,9))\n" +
            "    print(checkcode)";

    /**
     * RUBY_STRING
     */
    public static final String RUBY_STRING = "def fact(n)   \n" +
            "    if n == 0     \n" +
            "       1   \n" +
            "    else     \n" +
            "       n * fact(n-1)   \n" +
            "    end \n" +
            "end \n" +
            "print fact(ARGV[0].to_i), \"\\n\"   ";

    /**
     * CSHARP_STRING
     */
    public static final String CSHARP_STRING = "class Program\n" +
            " {\n" +
            " static void Main(string[] args)\n" +
            " {\n" +
            "  Transform t = new Transform();\n" +
            "  // t.GetVector().x = 10; 这边编译报错\n" +
            "  Vector v2 = t.GetVector();\n" +
            "  v2.x = 10;\n" +
            "  t.ShowV();\n" +
            "  Console.Read();\n" +
            " }\n" +
            " }\n" +
            " \n" +
            " struct Vector\n" +
            " {\n" +
            " public float x;\n" +
            " public float y;\n" +
            " public float z;\n" +
            " }\n" +
            " \n" +
            " class Transform\n" +
            " {\n" +
            " public Vector v;\n" +
            " //set\n" +
            " public void SetVector(Vector v)\n" +
            " {\n" +
            "  this.v = v;\n" +
            " }\n" +
            " //get\n" +
            " public Vector GetVector()\n" +
            " {\n" +
            "  return v;\n" +
            " }\n" +
            " \n" +
            " public void ShowV()\n" +
            " {\n" +
            "  Console.WriteLine(v.x + \"...\" + v.y + \"...\" + v.z);\n" +
            " }\n" +
            " }";

    /**
     * PHP_STRING
     */
    public static final String PHP_STRING = "function is_spam($text, $file, $split = ‘:‘, $regex = false){ \n" +
            "$handle = fopen($file, ‘rb‘); \n" +
            "$contents = fread($handle, filesize($file)); \n" +
            "fclose($handle); \n" +
            "$lines = explode(\"n\", $contents); \n" +
            "$arr = array(); \n" +
            "foreach($lines as $line){ \n" +
            "list($word, $count) = explode($split, $line); \n" +
            "if($regex) \n" +
            "$arr[$word] = $count; \n" +
            "else \n" +
            "$arr[preg_quote($word)] = $count; \n" +
            "} \n" +
            "preg_match_all(\"~\".implode(‘|‘, array_keys($arr)).\"~\", $text, $matches); \n" +
            "$temp = array(); \n" +
            "foreach($matches[0] as $match){ \n" +
            "if(!in_array($match, $temp)){ \n" +
            "$temp[$match] = $temp[$match] + 1; \n" +
            "if($temp[$match] >= $arr[$word]) \n" +
            "return true; \n" +
            "} \n" +
            "} \n" +
            "return false; \n" +
            "} \n" +
            "$file = ‘spam.txt‘; \n" +
            "$str = ‘This string has cat, dog word‘; \n" +
            "if(is_spam($str, $file)) \n" +
            "echo ‘this is spam‘; \n" +
            "else \n" +
            "echo ‘this is not spam‘; \n" +
            "ab:3 \n" +
            "dog:3 \n" +
            "cat:2 \n" +
            "monkey:2 ";

    /**
     * SQL
     */
    public static final String SQL_STRING = "CREATE  TABLE  ENROLLS\n" +
            "\n" +
            "        (SNO      NUMERIC(6,0)  NOT NULL\n" +
            "\n" +
            "        CNO     CHAR(4)  NOT NULL\n" +
            "\n" +
            "        GRADE   INT\n" +
            "\n" +
            "        PRIMARY KEY(SNO,CNO)\n" +
            "\n" +
            "        FOREIGN KEY(SNO) REFERENCES STUDENTS(SNO)\n" +
            "\n" +
            "        FOREIGN KEY(CNO) REFERENCES COURSES(CNO)\n" +
            "\n" +
            "        CHECK ((GRADE IS NULL) OR (GRADE BETWEEN 0 AND 100)))";

    /**
     * SQL
     */
    public static final String CPLUSPLUS_STRING = "using namespace std;\n" +
            " \n" +
            "extern int a, b;\n" +
            "extern int c;\n" +
            "extern float f;\n" +
            "int main()\n" +
            "{\n" +
            "\t//定义变量\n" +
            "\tint a, b;\n" +
            "\tint c;\n" +
            "\tfloat f;\n" +
            "\t\n" +
            "\t//实际初始化\n" +
            "\ta = 10;\n" +
            "\tb = 20;\n" +
            "\tc=a+b;\n" +
            "\t\n" +
            "\tcout << c << endl;\n" +
            "\tf = 70.0/3.0;\n" +
            "\tcout << f << endl;\n" +
            "\treturn 0;\n" +
            "}";
}
