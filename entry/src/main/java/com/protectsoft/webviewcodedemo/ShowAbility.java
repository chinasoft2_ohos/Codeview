/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.protectsoft.webviewcodedemo;

import com.protectsoft.webviewcode.Codeview;
import com.protectsoft.webviewcode.Settings;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.webengine.WebView;

/**
 * ShowAbility
 *
 * @since 2021-04-30
 */
public class ShowAbility extends Ability {
    private static final int NUM_20 = 20;
    private final int[] resIds = {
        ResourceTable.Id_btn_default,ResourceTable.Id_btn_agate,
        ResourceTable.Id_btn_OHOSSTUDIO,ResourceTable.Id_btn_ARDUINO_LIGHT,ResourceTable.Id_btn_ARTA,
        ResourceTable.Id_btn_ASCETIC,ResourceTable.Id_btn_ATELIER_DARK,ResourceTable.Id_btn_ATELIER_LIGHT,
        ResourceTable.Id_btn_ATELIER_FOREST_DARK,ResourceTable.Id_btn_DARKSTYLE,
        ResourceTable.Id_btn_DARKULA,ResourceTable.Id_btn_DOCCO,ResourceTable.Id_btn_FAR,
        ResourceTable.Id_btn_GITHUB,ResourceTable.Id_btn_GITHUBGIST,
        ResourceTable.Id_btn_GGCODE,ResourceTable.Id_btn_IDEA,ResourceTable.Id_btn_MAGULA,
        ResourceTable.Id_btn_OBSIDIAN,ResourceTable.Id_btn_XCODE};
    private final String[] styles = {
        Settings.WithStyle.DEFAULT,Settings.WithStyle.AGATE,
        Settings.WithStyle.OHOSSTUDIO,Settings.WithStyle.ARDUINO_LIGHT,Settings.WithStyle.ARTA,
        Settings.WithStyle.ASCETIC,Settings.WithStyle.ATELIER_DARK,
        Settings.WithStyle.ATELIER_LIGHT,Settings.WithStyle.ATELIER_FOREST_DARK,Settings.WithStyle.DARKSTYLE,
        Settings.WithStyle.DARKULA,Settings.WithStyle.DOCCO,
        Settings.WithStyle.FAR,Settings.WithStyle.GITHUB,Settings.WithStyle.GITHUBGIST,
        Settings.WithStyle.GGCODE,Settings.WithStyle.IDEA,
        Settings.WithStyle.MAGULA,Settings.WithStyle.OBSIDIAN,Settings.WithStyle.XCODE};
    private final Button[] buttons = new Button[NUM_20];
    private Text textStyle;
    private Text textAutoWrap;
    private boolean isMyAutoWrap = false;
    private WebView webView;
    private Intent mIntent;
    private int mPosition = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_show);
        DirectionalLayout directionalLayout = (DirectionalLayout)findComponentById(ResourceTable.Id_dl);
        webView = new WebView(getContext());
        webView.setComponentSize(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                DirectionalLayout.LayoutConfig.MATCH_PARENT);
        directionalLayout.addComponent(webView);
        mIntent = intent;
        initView();
    }

    private void initView() {
        textStyle = (Text) findComponentById(ResourceTable.Id_t_style);
        textAutoWrap = (Text) findComponentById(ResourceTable.Id_t_AutoWrap);
        for (int i = 0;i < NUM_20;i++) {
            Button button = (Button)findComponentById(resIds[i]);
            int finalI = i;
            button.setClickedListener(component -> setView(button.getText(), finalI,isMyAutoWrap));
            buttons[i] = button;
        }
        Button button = (Button)findComponentById(ResourceTable.Id_btn_AutoWrap);
        button.setClickedListener(component -> {
            isMyAutoWrap = !isMyAutoWrap;
            setView(buttons[mPosition].getText(),mPosition,isMyAutoWrap);
        });
        setView(buttons[mPosition].getText(),mPosition,isMyAutoWrap);
    }

    private void setView(String style,int position,boolean isAutoWrap) {
        mPosition = position;
        textStyle.setText(style);
        textAutoWrap.setText(String.valueOf(isAutoWrap));
        if (mIntent.getIntParam(Constants.SHOW_TYPE,1) == Constants.OTHER1_TYPE) {
            Codeview.with(getApplicationContext())
                    .setHtmlHeadContent("<style> table,tr,td {"
                            + " border: 1px solid black;"
                            + " }"
                            + ""
                            + "</style>")
                    .withHtml("<h1> h1 injected header</h1>")
                    .withText("this text is always wrap inside pre tags")
                    .withCode(Constants.OTHER1_STRING)
                    .withHtml("<h1> h1 header after code </h1>")
                    .withHtml("<table><tr><td> my html table </td></tr></table>")
                    .setStyle(styles[position])
                    .setLang(Settings.Lang.JAVASCRIPT)
                    .setAutoWrap(isAutoWrap)
                    .into(webView);
        } else {
            Codeview.with(getApplicationContext())
                    .withCode(mIntent.getStringParam(Constants.SHOW_CODE))
                    .setLang(mIntent.getStringParam(Constants.SHOW_LANG))
                    .setStyle(styles[position])
                    .setAutoWrap(isAutoWrap)
                    .into(webView);
        }
    }
}
