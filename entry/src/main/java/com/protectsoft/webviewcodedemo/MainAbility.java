/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.protectsoft.webviewcodedemo;

import com.protectsoft.webviewcode.Settings;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;

/**
 * MainAbility
 *
 * @since 2021-04-30
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Button btnDefalut = (Button)findComponentById(ResourceTable.Id_btn_defalut);
        btnDefalut.setClickedListener(component ->
                jumpAbilityByType(Constants.DEFALUT_TYPE,Constants.DEFALUT_STRING, Settings.Lang.JAVA));
        Button btnOther = (Button)findComponentById(ResourceTable.Id_btn_other);
        btnOther.setClickedListener(component ->
                jumpAbilityByType(Constants.OTHER_TYPE,Constants.OTHER_STRING, Settings.Lang.JAVA));
        Button btnOther1 = (Button)findComponentById(ResourceTable.Id_btn_other1);
        btnOther1.setClickedListener(component ->
                jumpAbilityByType(Constants.OTHER1_TYPE,Constants.OTHER1_STRING, Settings.Lang.JAVASCRIPT));
        Button btnPython = (Button)findComponentById(ResourceTable.Id_btn_python);
        btnPython.setClickedListener(component ->
                jumpAbilityByType(Constants.PYTHON_TYPE,Constants.PYTHON_STRING, Settings.Lang.PYTHON));
        Button btnJavascript = (Button)findComponentById(ResourceTable.Id_btn_javascript);
        btnJavascript.setClickedListener(component ->
                jumpAbilityByType(Constants.JAVASCRIPT_TYPE,Constants.OTHER1_STRING, Settings.Lang.JAVASCRIPT));
        Button btnRuby = (Button)findComponentById(ResourceTable.Id_btn_ruby);
        btnRuby.setClickedListener(component ->
                jumpAbilityByType(Constants.RUBY_TYPE,Constants.RUBY_STRING, Settings.Lang.RUBY));
        Button btnCs = (Button)findComponentById(ResourceTable.Id_btn_cs);
        btnCs.setClickedListener(component ->
                jumpAbilityByType(Constants.CSHARP_TYPE,Constants.CSHARP_STRING, Settings.Lang.CSHARP));
        Button btnPhp = (Button)findComponentById(ResourceTable.Id_btn_php);
        btnPhp.setClickedListener(component ->
                jumpAbilityByType(Constants.PHP_TYPE,Constants.PHP_STRING, Settings.Lang.PHP));
        Button btnSql = (Button)findComponentById(ResourceTable.Id_btn_sql);
        btnSql.setClickedListener(component ->
                jumpAbilityByType(Constants.SQL_TYPE,Constants.SQL_STRING, Settings.Lang.SQL));
        Button btnCpp = (Button)findComponentById(ResourceTable.Id_btn_cpp);
        btnCpp.setClickedListener(component ->
                jumpAbilityByType(Constants.CPLUSPLUS_TYPE,Constants.CPLUSPLUS_STRING, Settings.Lang.CPLUSPLUS));
    }

    private void jumpAbilityByType(int type,String code,String lang) {
        Intent intent = new Intent();
        intent.setParam(Constants.SHOW_TYPE, type);
        intent.setParam(Constants.SHOW_CODE, code);
        intent.setParam(Constants.SHOW_LANG, lang);
        Operation mOperation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(ShowAbility.class.getName())
                .build();
        intent.setOperation(mOperation);
        startAbility(intent);
    }
}
