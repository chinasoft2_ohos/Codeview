package com.protectsoft.webviewcode;

import ohos.agp.components.webengine.WebView;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class CodeViewTest {
    private final Codeview codeview = mock(Codeview.class);
    private final WebView webview = mock(WebView.class);
    @Test
    public void setLangTest(){
        codeview.setLang(Settings.Lang.JAVASCRIPT);
    }

    @Test
    public void setHtmlHeadContentTest(){
        codeview.setHtmlHeadContent("<style> table,tr,td {"
                + " border: 1px solid black;"
                + " }"
                + ""
                + "</style>");
    }

    @Test
    public void withHtmlTest(){
        codeview.withHtml("<h1> h1 injected header</h1>");
    }

    @Test
    public void withTextTest(){
        codeview.withText("this text is always wrap inside pre tags");
    }

    @Test
    public void withCodeTest(){
        codeview.withCode("function Constructor(v1,v2,v3)\n" + "{\n"
                + "  this.v1 = v1;\n"
                + "  this.v2 = v2;\n"
                + "  this.funk = function()\n"
                + "  {\n"
                + "    console.log(\"Test: \"+ v3 );\n"
                + "  }\n"
                + "}\n"
                + "\n"
                + "var obj1 = new Constructor(\"par1\",\"par2\",\"par3\");\n"
                + "var arr = [\"w1\",\"w2\",\"w3\",obj1];\n" + "\n"
                + "function f2()\n" + "{            \n"
                + "  obj1.funk(); //works ok\n"
                + "  console.log(\"test \"+tablica[3].funk.call() ); //doesn't work\n"
                + "}");
    }

    @Test
    public void setStyleTest(){
        codeview.setStyle(Settings.WithStyle.DEFAULT);
    }

    @Test
    public void setAutoWrapTest(){
        codeview.setAutoWrap(true);
    }

    @Test
    public void setIntoTest(){
        codeview.into(webview);
    }
}
