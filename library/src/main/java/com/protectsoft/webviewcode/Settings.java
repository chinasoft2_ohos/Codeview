package com.protectsoft.webviewcode;

/**
 * Settings
 *
 * @since 2021-04-30
 */
public final class Settings {
    // avoid instantiation
    private Settings() {
    }

    /**
     * Lang
     *
     * @since 2021-04-30
     */
    public static final class Lang {
        /**
         * JAVA
         */
        public static final String JAVA = "java";
        /**
         * PYTHON
         */
        public static final String PYTHON = "python";
        /**
         * JAVASCRIPT
         */
        public static final String JAVASCRIPT = "javascript";
        /**
         * RUBY
         */
        public static final String RUBY = "ruby";
        /**
         * CSHARP
         */
        public static final String CSHARP = "cs";
        /**
         * PHP
         */
        public static final String PHP = "php";
        /**
         * SQL
         */
        public static final String SQL = "sql";
        /**
         * CPLUSPLUS
         */
        public static final String CPLUSPLUS = "c++";
    }

    /**
     * WithStyle
     *
     * @since 2021-04-30
     */
    public static final class WithStyle {
        /**
         * DEFAULT
         */
        public static final String DEFAULT = HighlightLib.CSS_DEFAULT;
        /**
         * AGATE
         */
        public static final String AGATE = HighlightLib.CSS_AGATE;
        /**
         * OHOSSTUDIO
         */
        public static final String OHOSSTUDIO = HighlightLib.CSS_OHOSSTUDIO;
        /**
         * ARDUINO_LIGHT
         */
        public static final String ARDUINO_LIGHT = HighlightLib.CSS_ARDUINO;
        /**
         * ARTA
         */
        public static final String ARTA = HighlightLib.CSS_ARTA;
        /**
         * ASCETIC
         */
        public static final String ASCETIC = HighlightLib.CSS_ASCETIC;
        /**
         * ATELIER_DARK
         */
        public static final String ATELIER_DARK = HighlightLib.CSS_ATELIER_DARK;
        /**
         * ATELIER_LIGHT
         */
        public static final String ATELIER_LIGHT = HighlightLib.CSS_ATELIER_LIGHT;
        /**
         * ATELIER_FOREST_DARK
         */
        public static final String ATELIER_FOREST_DARK = HighlightLib.CSS_ATELIER_FOREST_DARK;
        /**
         * DARKSTYLE
         */
        public static final String DARKSTYLE = HighlightLib.CSS_DARKSTYLE;
        /**
         * DARKULA
         */
        public static final String DARKULA = HighlightLib.CSS_DARKULA;
        /**
         * DOCCO
         */
        public static final String DOCCO = HighlightLib.CSS_DOCCO;
        /**
         * FAR
         */
        public static final String FAR = HighlightLib.CSS_FAR;
        /**
         * GITHUB
         */
        public static final String GITHUB = HighlightLib.CSS_GITHUB;
        /**
         * GITHUBGIST
         */
        public static final String GITHUBGIST = HighlightLib.CSS_GIHUBGIST;
        /**
         * GGCODE
         */
        public static final String GGCODE = HighlightLib.CSS_GGCODE;
        /**
         * IDEA
         */
        public static final String IDEA = HighlightLib.CSS_IDEA;
        /**
         * MAGULA
         */
        public static final String MAGULA = HighlightLib.CSS_MAGULA;
        /**
         * OBSIDIAN
         */
        public static final String OBSIDIAN = HighlightLib.CSS_OBSIDIAN;
        /**
         * XCODE
         */
        public static final String XCODE = HighlightLib.CSS_XCODE;
    }

    /**
     * not implemented yet
     *
     * @since 2021-04-30
     */
    public static final class MimeType {
        /**
         * TEXT_HTML
         */
        public static final String TEXT_HTML = "text/html";
        /**
         * TEXT_PLAIN
         */
        public static final String TEXT_PLAIN = "text/plain";
    }

    /**
     * not implemented yet
     *
     * @since 2021-04-30
     */
    public static final class Charset {
        /**
         * UTF_8
         */
        public static final String UTF_8 = "utf-8";
    }

    /**
     * not implemented yet
     *
     * @since 2021-04-30
     */
    public static final class TextWrap {
        /**
         * PRE_WRAP
         */
        public static final String PRE_WRAP = "pre-wrap";
    }
}
