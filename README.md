# Codeview
#### 项目介绍
- 项目名称：Codeview
- 所属系列：openharmony的第三方组件适配移植
- 功能：代码块高亮显示。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 2

#### 效果演示

![输入图片说明](https://gitee.com/chinasoft2_ohos/Codeview/raw/master/gif/demo.gif  "demo.gif")

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```gradle
allprojects {
	repositories {
		maven {
			url 'https://s01.oss.sonatype.org/content/repositories/releases/'
		}
	}
}
```
2.在entry模块的build.gradle文件中，
```gradle
dependencies {
	implementation('com.gitee.chinasoft_ohos:Codeview:1.0.0')
	......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. 创建一个WebView对象，添加到页面中:
	```java
    DirectionalLayout directionalLayout = (DirectionalLayout)findComponentById(ResourceTable.Id_dl);
    webView = new WebView(getContext());
    webView.setComponentSize(DirectionalLayout.LayoutConfig.MATCH_PARENT,
            DirectionalLayout.LayoutConfig.MATCH_PARENT);
    directionalLayout.addComponent(webView);
	```
2. 基本用法:
	```java
	String code = "public static void main(String[] args) { \n" +
					"\n" +
					"//comments\n" +
					"   for(int i =0; i < 10; i++) {\n" +
					"       addnum();\n" +
					"   }\n" +
					"\n" +
					"}\n";
									
	Codeview.with(getApplicationContext())
			.withCode(code)
			.into(webview);
	```
	
3. 设置css样式和语言类型:
	```java 
	String code = "public static void main(String[] args) { \n" +
                "\n" +
                "//comments\n" +
                "   for(int i =0; i < 10; i++) {\n" +
                "       addnum();\n" +
                "   }\n" +
                "\n" +
                "}\n";
								
	Codeview.with(getApplicationContext())
		.withCode(code)
		.setStyle(Settings.WithStyle.DARKULA)
        .setLang(Settings.Lang.JAVA)
		.into(webview);
	```
4. 注入html头部以及文本:.
	```java
    String code = "function Constructor(v1,v2,v3)\n" +
                "{\n" +
                "  this.v1 = v1;\n" +
                "  this.v2 = v2;\n" +
                "  this.funk = function()\n" +
                "  {\n" +
                "    console.log(\"Test: \"+ v3 );\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "var obj1 = new Constructor(\"par1\",\"par2\",\"par3\");\n" +
                "var arr = [\"w1\",\"w2\",\"w3\",obj1];\n" +
                "\n" +
                "function f2()\n" +
                "{            \n" +
                "  obj1.funk(); //works ok\n" +
                "  console.log(\"test \"+tablica[3].funk.call() ); //doesn't work\n" +
                "}";
                
                
    Codeview.with(getApplicationContext())
        .setHtmlHeadContent("<style> table,tr,td {" +
                        " border: 1px solid black;" +
                        " }" +
                        "" +
                        "</style>")
        .withHtml("<h1> h1 injected header</h1>")
        .withText("this text is always wrap inside pre tags")
        .withCode(code)
        .withHtml("<h1> h1 header after code </h1>")
        .withHtml("<table><tr><td> my html table </td></tr></table>")
        .setStyle(Settings.WithStyle.DARKSTYLE)
        .setLang(Settings.Lang.JAVASCRIPT)
        .setAutoWrap(true)
        .into(webView);
	```
	
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
MIT License

Copyright (c) 2016 Avraam Piperidis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```